from django import forms
from .models import Jadwal
from django.forms import ModelForm


class JadwalForm(ModelForm):
    class Meta:
        required_css_class = 'required'
        model = Jadwal
        fields = ['namaKegiatan','tempat','tanggal','jam','kategori']
        labels = {
            'namaKegiatan':'Nama Kegiatan','tempat': 'Tempat','tanggal': 'Tanggal', 'jam': 'Jam', 'kategori':'Kategori'
        }
        widgets = {
            'namaKegiatan': forms.TextInput(attrs={'class': 'input',
                                               'type': 'text',
                                               'placeholder': 'e.g. Ngoding'}),
            'tempat': forms.TextInput(attrs={'class': 'input',
                                               'type': 'text',
                                               'placeholder': 'e.g. Sekre Square Fasilkom UI'}),
            'tanggal': forms.DateInput(attrs={'class': 'input',
                                            'type': 'date'}),
            'jam': forms.TimeInput(attrs={'class': 'input',
                                           'type': 'time'}),
            'kategori': forms.Select(attrs={'class': 'input'})
        }
