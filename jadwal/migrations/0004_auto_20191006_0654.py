# Generated by Django 2.2.5 on 2019-10-05 23:54

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('jadwal', '0003_auto_20191004_2018'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jadwal',
            name='kategori',
            field=models.CharField(
                choices=[('UJIAN', 'Ujian'), ('TUGAS', 'Tugas'), ('RAPAT', 'Rapat'), ('REFRESHING', 'Refreshing'),
                         ('TIDUR', 'Tidur')], default='TIDUR', max_length=20),
        ),
    ]
