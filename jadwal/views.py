from django.shortcuts import render, redirect
from .forms import JadwalForm
from .models import Jadwal

def jadwalList(request):
    if request.method == 'POST':
        form = JadwalForm(request.POST)
        if 'pk' in request.POST:
            Jadwal.objects.filter(pk=request.POST['pk']).delete()
            return redirect('/jadwal/')
        if form.is_valid():
            form.save()
            return redirect('/jadwal/')
    else:
        form = JadwalForm()

    jadwals = Jadwal.objects.all()
    content = {'title':'Jadwal',
               'jadwals':jadwals,
               'form':form}
    return render(request, 'jadwal.html',content)