from django.db import models

class Jadwal(models.Model):
    KATEGORI_CHOICES = [
        ('UJIAN', 'Ujian'),
        ('TUGAS','Tugas'),
        ('RAPAT','Rapat'),
        ('REFRESHING', 'Refreshing'),
        ('TIDUR','Tidur'),
    ]

    namaKegiatan = models.CharField(max_length=50)
    tanggal = models.DateField()
    jam = models.TimeField()
    tempat = models.CharField(max_length=50)
    kategori = models.CharField(max_length=20,choices=KATEGORI_CHOICES,default='TIDUR')