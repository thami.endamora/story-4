from django.contrib import admin
from django.urls import path, include
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('',views.index),
    path('about/',views.about),
    path('education/',views.education),
    path('experience/',views.experience),
    path('contact/',views.contact),
    path('extra/',views.extra),
    path('jadwal/', include('jadwal.urls')),

    path('admin/', admin.site.urls),
]

urlpatterns += staticfiles_urlpatterns()