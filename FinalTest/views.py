from django.shortcuts import render

def index(request):
    return render(request, 'index.html')

def about(request):
    return render(request, 'about.html')

def education(request):
    return render(request, 'edu.html')

def experience(request):
    return render(request, 'exp.html')

def contact(request):
    return render(request, 'contact.html')

def extra(request):
    return render(request, 'extra.html')